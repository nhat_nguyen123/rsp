import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as dart_image_lib;

class ImageManipulation {
  static const _smallImagesFolderName = 'small_images';

  /// Save image from an [imageUrl] and then return path to that image.
  static Future<String> saveImageFromNetwork({required String imageUrl}) async {
    final ByteData imageData =
        await NetworkAssetBundle(Uri.parse(imageUrl)).load("");
    final Uint8List bytes = imageData.buffer.asUint8List();
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
    String demoPath = appDocumentsDirectory.path + '/demoImage.png';
    File imageFile = await File(demoPath).writeAsBytes(bytes);
    return imageFile.path;
  }

  /// Crop a image from an [imageUri] and then return the cropped file.
  static Uint8List? cropImageFromImageBytes(
      {required Uint8List imageBytes, required Rect rect}) {
    final dart_image_lib.Image? image = dart_image_lib.decodeImage(imageBytes);

    if (image == null) {
      //ignore: avoid_print
      print('Error when saving image from network.');
      return null;
    }

    final dart_image_lib.Image croppedImage = dart_image_lib.copyCrop(
      image,
      rect.topLeft.dx.round(),
      rect.topLeft.dy.round(),
      rect.width.round(),
      rect.height.round(),
    );
    return Uint8List.fromList(dart_image_lib.encodePng(croppedImage));
  }

  static Future<File> saveImageBytesToFile({required Uint8List bytes}) async {
    imageCache?.clear();
    Directory appDocumentDirectory = await getApplicationDocumentsDirectory();
    final location = '${appDocumentDirectory.path}/$_smallImagesFolderName';
    if (!(await Directory(location).exists())) {
      await Directory(location).create();
    }
    File savedFile = await File('$location/piece.png').writeAsBytes(bytes);
    return savedFile;
  }

  /// Source image just supports .png and .jpg file at this moment.
  static Future<Size?> getSourceImageSize({
    required List<int> bytes,
  }) async {
    dart_image_lib.Image? imageFromBytes = dart_image_lib.decodeImage(bytes);
    Size? imageSize = imageFromBytes != null
        ? Size(
            imageFromBytes.width.toDouble(), imageFromBytes.height.toDouble())
        : null;

    return imageSize;
  }
}

class ImageFrame {
  final int x;
  final int y;
  final int width;
  final int height;
  ImageFrame({
    required this.x,
    required this.y,
    required this.width,
    required this.height,
  });
}
