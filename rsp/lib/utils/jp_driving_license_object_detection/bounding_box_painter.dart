import 'package:flutter/material.dart';
import 'package:flutter_license_object_detection/flutter_license_object_detection.dart';

class BoundingBoxPainter extends CustomPainter {
  Size imageSize;
  final List<LicenseInfoPrediction> predictions;
  int stroke = 1;

  BoundingBoxPainter({
    required this.imageSize,
    required this.predictions,
  });

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    for (final prediction in predictions) {
      final paint = Paint()
        ..color = Colors.red
        ..strokeWidth = 2
        ..strokeCap = StrokeCap.round;

      final double imageWidth = imageSize.width;
      final double imageHeight = imageSize.height;

      final double viewHeight = size.height;
      final double viewWidth = size.width;

      final rect = prediction.rect;

      final Offset tl = Offset(
        rect.topLeft.dx * viewWidth / imageWidth,
        rect.topLeft.dy * viewHeight / imageHeight,
      );
      final Offset tr = Offset(
        rect.topRight.dx * viewWidth / imageWidth,
        rect.topRight.dy * viewHeight / imageHeight,
      );
      final Offset br = Offset(
        rect.bottomRight.dx * viewWidth / imageWidth,
        rect.bottomRight.dy * viewHeight / imageHeight,
      );
      final Offset bl = Offset(
        rect.bottomLeft.dx * viewWidth / imageWidth,
        rect.bottomLeft.dy * viewHeight / imageHeight,
      );

      canvas.drawLine(tl, tr, paint);
      canvas.drawLine(tr, br, paint);
      canvas.drawLine(br, bl, paint);
      canvas.drawLine(bl, tl, paint);

      final textSpan = TextSpan(
        text: prediction.type.toString().split('.').last,
        style: TextStyle(
          color: Colors.blue,
          fontWeight: FontWeight.bold,
          backgroundColor: Colors.white,
        ),
      );
      final textPainter = TextPainter(
        text: textSpan,
        textDirection: TextDirection.ltr,
      );

      final boxTopCenter = Offset(tl.dx + (tr.dx - tl.dx) / 2, tl.dy);

      textPainter.layout();
      textPainter.paint(
        canvas,
        Offset(
          boxTopCenter.dx - textPainter.width / 2,
          boxTopCenter.dy - textPainter.height / 2,
        ),
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
