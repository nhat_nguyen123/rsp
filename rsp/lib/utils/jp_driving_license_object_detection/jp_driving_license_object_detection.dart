import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_license_object_detection/flutter_license_object_detection.dart';
import 'package:rsp/utils/image_manipulation/image_manipulation.dart';
import 'package:screenshot/screenshot.dart';

class JPDrivingLicenseObjectDetection {
  static Future<List<LicenseInfoPrediction>> getPredictions({required Uint8List imageByBytes}) async {
    return await FlutterLicenseObjectDetection.processImage(imageByBytes);
  }

  static Future<Map<LicenseInfoType, List<Uint8List>>> getDetectedImages({required Uint8List imageByBytes}) async {
    final List<LicenseInfoPrediction> predictions = await getPredictions(imageByBytes: imageByBytes);
    final Map<LicenseInfoType, List<Uint8List>> smallImages = {};

    //TODO: Testing and ignore this for loop.
    /*
    for (final prediction in predictions) {
      Uint8List? smallImage = ImageManipulation.cropImageFromImageBytes(
        imageBytes: imageByBytes,
        rect: prediction.rect,
      );

      if (smallImage != null) {
        // add extrat space and text to image
        Uint8List paddingImage = await addSpaceForImage(smallImage);

        if (smallImages.containsKey(prediction.type)) {
          smallImages[prediction.type]?.add(paddingImage);
        } else {
          smallImages[prediction.type] = [paddingImage];
        }
      }
    }
		*/

    // Run adding space to images in parallel.
    await Future.wait(predictions.map((prediction) async {
      Uint8List? smallImage = ImageManipulation.cropImageFromImageBytes(
        imageBytes: imageByBytes,
        rect: prediction.rect,
      );

      if (smallImage != null) {
        // add extrat space and text to image
        Uint8List paddingImage = await addSpaceForImage(smallImage);

        if (smallImages.containsKey(prediction.type)) {
          smallImages[prediction.type]?.add(paddingImage);
        } else {
          smallImages[prediction.type] = [paddingImage];
        }
      }
    }));

    return smallImages;
  }

  static Future<Uint8List> addSpaceForImage(Uint8List smallImage) async {
    ScreenshotController screenshotController = ScreenshotController();
    Uint8List paddingImage = await screenshotController.captureFromWidget(
      Container(
        padding: EdgeInsets.all(20),
        height: 200,
        child: Column(
          children: [
            Text(
              'HELLO',
              style: TextStyle(fontSize: 30),
            ),
            Image.memory(smallImage, height: 50),
          ],
        ),
      ),
    );
    return paddingImage;
  }
}

class TypeImagePair {
  final LicenseInfoType type;
  final Uint8List? bytes;
  TypeImagePair({required this.type, this.bytes});
}
