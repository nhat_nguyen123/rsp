import 'dart:io';
import 'dart:typed_data';

import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:rsp/utils/image_manipulation/image_manipulation.dart';
import 'package:image/image.dart' as dart_image_lib;

class GoogleMLTextRecognition {
  static const minDetectionWidth = 33;
  static const minDetectionHeight = 33;

  static Future<List<String>> detectingTextsFromImages(
      List<Uint8List> smallImages) async {
    final detectedTexts = <String>[];
    for (final smallImage in smallImages) {
      final smallImageText =
          await detectingTextFromBytes(imageBytes: smallImage);
      if (smallImageText != null) {
        detectedTexts.addAll(smallImageText);
      }
    }
    return detectedTexts;
  }

  static Future<List<String>> processDetectingTextFromFilePath({
    required String imagePath,
  }) async {
    final inputImage = InputImage.fromFilePath(imagePath);
    return await _detectingText(inputImage: inputImage);
  }

  static Future<List<String>?> detectingTextFromBytes({
    required Uint8List imageBytes,
  }) async {
    // !important: image hieght must higher than 32 for accepting detection in Google ML.
    Uint8List? imageByBytesWithIncreasedHeight =
        _increaseImageSizeForDetection(imageBytes);
    if (imageByBytesWithIncreasedHeight == null) return null;
    File imageFile = await ImageManipulation.saveImageBytesToFile(
        bytes: imageByBytesWithIncreasedHeight);
    final inputImage = InputImage.fromFile(imageFile);
    return await _detectingText(inputImage: inputImage);
  }

  static Uint8List? _increaseImageSizeForDetection(Uint8List imageBytes) {
    final image = dart_image_lib.decodeImage(imageBytes);
    if (image == null) return null;

    final expectedHeight =
        image.height < minDetectionHeight ? minDetectionHeight : image.height;
    final expectedWidth =
        image.width < minDetectionWidth ? minDetectionWidth : image.width;
    final higherImage = dart_image_lib.copyResize(image,
        width: expectedWidth, height: expectedHeight);

    return Uint8List.fromList(dart_image_lib.encodePng(higherImage));
  }

  static Future<List<String>> _detectingText({
    required InputImage inputImage,
  }) async {
    final textDetector = GoogleMlKit.vision.textDetectorV2();
    final RecognisedText recognisedText = await textDetector.processImage(
      inputImage,
      script: TextRecognitionOptions.JAPANESE,
    );
    final List<TextBlock> blocks = recognisedText.blocks;
    return _getRecognizedLines(blocks);
  }

  static List<String> _getRecognizedLines(List<TextBlock> blocks) {
    final List<String> recognizedLines = <String>[];
    for (final block in blocks) {
      for (final line in block.lines) {
        recognizedLines.add(line.text);
      }
    }
    return recognizedLines;
  }
}
