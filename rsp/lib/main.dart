import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:rsp/modules/pages/cropper_screen.dart';
import 'package:rsp/modules/pages/image_screen.dart';
import 'package:rsp/modules/pages/output_screen.dart';
import 'package:camera/camera.dart';

late List<CameraDescription> cameraList;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  cameraList = await availableCameras();
  runApp(const RspApp());
}

class RspApp extends StatelessWidget {
  const RspApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'RSP APP DEMO', initialRoute: '/', routes: {
      '/': (context) => ImageScreen(),
      '/image': (context) => CropperScreen(
            imageBytes: Uint8List.fromList([]),
          ),
      '/info_output': (context) => OutputScreen(
            imageBytes: Uint8List.fromList([]),
            detectedTexts: {},
          ),
    });
  }
}
