import 'package:rsp/common/enums/jp_driving_license_enum.dart';

abstract class JapanDrivingLicenseEnumRegExp {
  static Map<JPDrivingLicenseEnum, RegExp> patterns = {
    JPDrivingLicenseEnum.lastAndFirstName: RegExp(""),
    JPDrivingLicenseEnum.dateOfBirth:
        RegExp(r".*(昭和\d{2}\s*年\s*\d{1}\s*月\d{2}\s*日生).*"),
    JPDrivingLicenseEnum.address: RegExp(""),
    JPDrivingLicenseEnum.dateIssueOfCard:
        RegExp(r"(平成\d{2}\s*年\s*\d{2}\s*月\s*\d{2}\s*日)\s*\d{4}"),
    JPDrivingLicenseEnum.issueOfCardNumber:
        RegExp(r"(平成\d{2}\s*年\s*\d{2}\s*月\s*\d{2}\s*日)\s*(\d{4})"),
    JPDrivingLicenseEnum.expiredDate:
        RegExp(r"(平成\d{2}年\d{2}月\d{2}日\s*まで有効)[一-龯]*"),
    JPDrivingLicenseEnum.conditions: RegExp("([一-龯]{3})"),
    JPDrivingLicenseEnum.licenseNumber: RegExp(r"\d{6,}"),
    JPDrivingLicenseEnum.dateOfFirstIssueOfCommercialLicenses:
        RegExp(r"(平版\d{2}年\d{2}月\d{2}日)"),
    JPDrivingLicenseEnum.dateOfFirstIssueOfOtherLicenses:
        RegExp(r"(平版\d{2}年\d{2}月\d{2}日)"),
    JPDrivingLicenseEnum.dateOfFirstIssueOfMotocycleLicenses:
        RegExp(r"(平版\d{2}年\d{2}月\d{2}日)"),
    JPDrivingLicenseEnum.issuingAuthority: RegExp("[一-龯]+"),
  };
}
