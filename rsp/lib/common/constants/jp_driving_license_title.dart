class JPDrivingLicenseTitle {
  static const String dateOfBirth = "年 月 日生";
  static const String lastAndFirstName = "氏名";
  static const String residence = "本籍";
  static const String address = "住所";
  static const String dateIssueOfCard = "交付";
  static const String expiredDate = "年 月 日まで有効";
  static const String conditions = "免許の 条件等";
  static const String superior = "上長";
  static const String licenseNumber = "番号";
  static const String dateOfFirstIssueOfMotocycleLicenses = "二•小•原  ";
  static const String dateOfFirstIssueOfOtherLicenses = "他";
  static const String dateOfFirstIssueOfCommercialLicenses = "二種";
  static const String validCategoriesFirstPart = "種類";
  static const String validCategoriesSecondPart = "類";
  static const String issuingAuthority = "公安委員会";
}
