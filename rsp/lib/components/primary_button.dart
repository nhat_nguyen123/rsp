import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String name;
  final bool isProcessing;
  final Icon? icon;

  const PrimaryButton({
    Key? key,
    required this.onPressed,
    required this.name,
    required this.isProcessing,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final icon = this.icon;
    return ElevatedButton(
      onPressed: isProcessing ? null : () => onPressed(),
      child: isProcessing
          ? SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(
                color: Colors.white,
              ),
            )
          : Center(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: icon != null
                      ? [icon, SizedBox(width: 10), Text(name)]
                      : [Text(name)])),
      style: ElevatedButton.styleFrom(
        fixedSize: Size(350, 50),
        textStyle: const TextStyle(fontSize: 20),
      ),
    );
  }
}
