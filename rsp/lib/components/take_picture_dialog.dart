import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as image_lib;

class TakePictureDialog extends StatefulWidget {
  final void Function(Uint8List imageBytes) onImageTaken;

  const TakePictureDialog({Key? key, required this.onImageTaken})
      : super(key: key);

  @override
  State<TakePictureDialog> createState() => _TakePictureDialogState();
}

class _TakePictureDialogState extends State<TakePictureDialog> {
  CameraController? _cameraController;

  bool _isCameraActive = false;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      final cameraList = await availableCameras();
      final _cameraController = CameraController(
          cameraList[0], ResolutionPreset.ultraHigh,
          enableAudio: false);

      await _cameraController.initialize();
      setState(() {
        _isCameraActive = true;
      });
    });
  }

  @override
  void dispose() {
    _cameraController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_isCameraActive) {
      return SizedBox();
    }

    return AlertDialog(
      content: CameraPreview(
        _cameraController!,
        child: LayoutBuilder(builder: (context, constraints) {
          final cardWidth = constraints.maxHeight * 4 / 5;

          return Center(
            child: RotatedBox(
              quarterTurns: -3,
              child: Container(
                width: cardWidth,
                height: cardWidth / 1.6,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                    width: 3,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: Text(
                    'YOUR CARD HERE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      // ignore: prefer_const_literals_to_create_immutables
                      shadows: [
                        Shadow(
                          offset: Offset(1, 1),
                          blurRadius: 1,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
      actions: [
        ElevatedButton(
          onPressed: () async {
            setState(() {
              _isLoading = true;
            });
            final xFile = await _cameraController?.takePicture();
            final bytes = await xFile?.readAsBytes();

            final rotatedBytes = _rotateImage(<String, dynamic>{
              'imageBytes': bytes,
              'angle': -90.0,
            });

            setState(() {
              _isCameraActive = false;
              _isLoading = false;
            });

            Navigator.of(context).pop();

            if (rotatedBytes == null) {
              return;
            }

            widget.onImageTaken(Uint8List.fromList(rotatedBytes));
          },
          child: Center(
            child: !_isLoading
                ? Text('Take Picture')
                : SizedBox(
                    height: 20,
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    ),
                  ),
          ),
        ),
      ],
    );
  }
}

Uint8List? _rotateImage(Map<String, dynamic> input) {
  final Uint8List imageBytes = input['imageBytes'] as Uint8List;
  final double angle = input['angle'] as double;

  final image = image_lib.decodeImage(imageBytes);

  if (image == null) {
    return null;
  }

  final rotatedImage = image_lib.copyRotate(image, angle);
  final rotatedImageBytes = image_lib.encodeJpg(rotatedImage);

  return Uint8List.fromList(rotatedImageBytes);
}
