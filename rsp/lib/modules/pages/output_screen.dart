import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_license_object_detection/flutter_license_object_detection.dart';
import 'package:rsp/common/constants/jp_driving_license_title.dart';

class OutputScreen extends StatelessWidget {
  final Map<LicenseInfoType, List<String>> detectedTexts;
  final Uint8List imageBytes;

  const OutputScreen({
    Key? key,
    required this.detectedTexts,
    required this.imageBytes,
  }) : super(key: key);

  String _getLicenseOwnerName() {
    final targetTexts = detectedTexts[LicenseInfoType.nameAndDob];
    if (targetTexts != null && targetTexts.length < 3 && targetTexts.isNotEmpty) {
      return _cleanLicenseOwnerName(targetTexts[0]);
    }
    int start = 0;
    int end = 0;
    if (targetTexts == null) return '';
    for (int i = 0; i < targetTexts.length; i++) {
      // Consider case name is latin
      String? latinName = RegExp(r'(([A-Z]+[a-z]*\s*)+)').stringMatch(targetTexts[i]);
      if (latinName != null) {
        return latinName;
      }

      // Consider case name is japanese
      if (targetTexts[i].contains('氏') || targetTexts[i].contains('名')) {
        start = i;
      }
      if (targetTexts[i].contains('昭') ||
          targetTexts[i].contains('和') ||
          targetTexts[i].contains('年') ||
          targetTexts[i].contains('月') ||
          targetTexts[i].contains('日')) {
        end = i;
      }
    }
    if (end - start >= 2) {
      final targetText = targetTexts.sublist(start + 1, end).join(" ");
      return _cleanLicenseOwnerName(targetText);
    } else if (end - start == 1) {
      final targetText = targetTexts.sublist(start, end).join(" ");
      return _cleanLicenseOwnerName(targetText);
    }
    return '';
  }

  String _cleanLicenseOwnerName(String targetText) {
    targetText = targetText.replaceAll('|', '');
    if (targetText.contains('名')) {
      targetText = targetText.substring(targetText.indexOf('名') + 1);
    }
    if (targetText.contains('昭')) {
      targetText = targetText.substring(0, targetText.indexOf('昭'));
    }
    return targetText;
  }

  String _getResidence() {
    // if residence is recognized with nameAndDob
    final targetTexts = detectedTexts[LicenseInfoType.nameAndDob];
    if (targetTexts == null) return '';
    for (int i = 0; i < targetTexts.length; i++) {
      if (targetTexts[i].contains('本籍')) {
        List<String> listContainsResidence = targetTexts.sublist(i);
        String joinedText = listContainsResidence.join(' ');
        joinedText = _clearRedundantSign(joinedText);
        joinedText = joinedText.replaceAll('本', '');
        joinedText = joinedText.replaceAll('籍', '');
        return joinedText;
      }
    }

    // add case that residence box can be recognized.
    final targetTexts2 = detectedTexts[LicenseInfoType.residence];
    if (targetTexts2 == null) return '';
    String joinedText = targetTexts2.join(' ');
    joinedText = _clearRedundantSign(joinedText);
    joinedText = joinedText.replaceAll('本', '');
    joinedText = joinedText.replaceAll('籍', '');

    return joinedText;
  }

  String _getDateOfBirth() {
    final targetTexts = detectedTexts[LicenseInfoType.nameAndDob];
    if (targetTexts == null) return '';
    for (final text in targetTexts) {
      if (text.contains(RegExp(r'[0-9]'))) {
        if (text.length > 15) {
          return _getTextInArrayOfTextsWithPattern(detectedTexts[LicenseInfoType.nameAndDob],
              r'([一-龯]{2}\s?\d{1,2}\s?年\s?\d{1,2}\s?月\s?\d{1,2}\s?[一-龯]{1,2})');
        }
        return _clearRedundantSign(text);
      }
    }
    return '';
  }

  /// Remove all signs including '|', '(', ')'.
  String _clearRedundantSign(String text) {
    String newText = text.replaceAll(')', '');
    newText = newText.replaceAll('(', '');
    newText = newText.replaceAll('|', '');
    return newText;
  }

  String _getAddress() {
    final targetTexts = detectedTexts[LicenseInfoType.address];
    if (targetTexts == null) return '';
    String text = targetTexts.join('');
    text = _clearRedundantSign(text);
    if (text.contains('所')) {
      text = text.substring(text.indexOf('所') + 1);
    }
    return text;
  }

  String _getDeliveryDate() {
    final targetTexts = detectedTexts[LicenseInfoType.deliveryDate];
    if (targetTexts == null) return '';
    String text = targetTexts.join(' ');
    text = _clearRedundantSign(text);
    if (text.contains('付')) {
      text = text.substring(text.indexOf('付') + 1);
    }
    if (text.length > 20 && text.contains('平')) {
      text = text.substring(text.lastIndexOf('平'));
    }
    return text;
  }

  String _getValidUntil() {
    // 平成22年05月27日まで有効
    final targetTexts = detectedTexts[LicenseInfoType.validUntil];
    if (targetTexts == null) return '';
    for (final text in targetTexts) {
      if (text.contains('効') || text.contains('有') || text.contains('で') || text.contains('ま')) {
        if (text[0] == '|') {
          return text.substring(1);
        }
        return text;
      }
    }
    return '';
  }

  String _getConditions() {
    final targetTexts = detectedTexts[LicenseInfoType.conditions];
    if (targetTexts == null) return '';
    final listTextsAfterClearTitles = <String>[];
    for (final text in targetTexts) {
      if (!text.contains('免許の') && !text.contains('条件等')) {
        listTextsAfterClearTitles.add(text);
      }
    }
    return listTextsAfterClearTitles.join(' ');
  }

  String _getSuperior() {
    return _getTextInArrayOfTextsWithPattern(detectedTexts[LicenseInfoType.superior], r'([一-龯]{1,})');
  }

  String _getTextInArrayOfTextsWithPattern(List<String>? texts, String pattern) {
    if (texts == null) return '';
    final targetTexts = texts;
    RegExp regExp = RegExp(pattern);
    for (final text in targetTexts) {
      String? matchStr = regExp.stringMatch(text);
      if (matchStr != null) {
        return matchStr;
      }
    }
    return '';
  }

  String _getLicenseNumber() {
    final targetTexts = detectedTexts[LicenseInfoType.licenseNumber];
    if (targetTexts == null) return '';
    RegExp regExp = RegExp(r'(\d{3,})');
    for (final text in targetTexts) {
      if (text.length > 3 && regExp.hasMatch(text)) {
        String? match = regExp.stringMatch(text);
        return match ?? '';
      }
    }
    return '';
  }

  List<String> _getFirstIssueDates() {
    final targetTexts = detectedTexts[LicenseInfoType.firstIssueDates];
    if (targetTexts == null) return [];
    List<String> result = [];
    for (var text in targetTexts) {
      if (text.contains('平') || text.contains('成') || text.contains('年') || text.contains('月')) {
        text = _clearRedundantSign(text);

        // maybe if will recognized the border left to 1.
        if (text[0] == '1') {
          text = text.substring(1);
        }
        result.add(text);
      }
    }
    return result;
  }

  String _getIssuingAuthority() {
    final targetTexts = detectedTexts[LicenseInfoType.issuingAuthority];
    if (targetTexts == null) return '';
    RegExp regExp = RegExp(r'([一-龯]+)');
    for (final text in targetTexts) {
      if (!text.contains('公安委員会') && regExp.hasMatch(text)) {
        return text;
      }
    }
    return '';
  }

  _removeHELLO() {
    for (final listOfText in detectedTexts.values) {
      while (listOfText.contains('HELLO')) {
        listOfText.remove('HELLO');
      }
    }
  }

  Map<String, String> _getLicenseInfo() {
    // !important: Remove all 'HELLO' in every detectedTexts
    _removeHELLO();

    final lastAndFirstName = _getLicenseOwnerName();
    final dateOfBirth = _getDateOfBirth();
    final residence = _getResidence();
    final address = _getAddress();
    final deliveryDate = _getDeliveryDate();
    final validUntil = _getValidUntil();
    final conditions = _getConditions();
    final superior = _getSuperior();
    final licenseNumber = _getLicenseNumber();
    final firstIssueDates = _getFirstIssueDates();
    final issuingAuthority = _getIssuingAuthority();

    Map<String, String> licenseInfo = {};
    licenseInfo[JPDrivingLicenseTitle.lastAndFirstName] = lastAndFirstName;
    licenseInfo[JPDrivingLicenseTitle.dateOfBirth] = dateOfBirth;
    licenseInfo[JPDrivingLicenseTitle.residence] = residence;
    licenseInfo[JPDrivingLicenseTitle.address] = address;
    licenseInfo[JPDrivingLicenseTitle.dateIssueOfCard] = deliveryDate;
    licenseInfo[JPDrivingLicenseTitle.expiredDate] = validUntil;
    licenseInfo[JPDrivingLicenseTitle.conditions] = conditions;
    licenseInfo[JPDrivingLicenseTitle.superior] = superior;
    licenseInfo[JPDrivingLicenseTitle.licenseNumber] = licenseNumber;
    licenseInfo[JPDrivingLicenseTitle.dateOfFirstIssueOfMotocycleLicenses] =
        firstIssueDates.isNotEmpty ? firstIssueDates[0] : '';
    licenseInfo[JPDrivingLicenseTitle.dateOfFirstIssueOfOtherLicenses] =
        firstIssueDates.length > 1 ? firstIssueDates[1] : '';
    licenseInfo[JPDrivingLicenseTitle.dateOfFirstIssueOfCommercialLicenses] =
        firstIssueDates.length > 2 ? firstIssueDates[2] : '';
    licenseInfo[JPDrivingLicenseTitle.issuingAuthority] = issuingAuthority;

    return licenseInfo;
  }

  @override
  Widget build(BuildContext context) {
    final licenseInfo = _getLicenseInfo();

    return Scaffold(
      appBar: AppBar(title: Text('Detected Information')),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AspectRatio(
                  aspectRatio: 14 / 9,
                  child: SizedBox(
                    width: double.infinity,
                    child: Image.memory(imageBytes),
                  ),
                ),
                SizedBox(height: 10),
                Table(
                  columnWidths: const {
                    0: FlexColumnWidth(
                      (100),
                    ),
                  },
                  defaultColumnWidth: FlexColumnWidth(
                    (MediaQuery.of(context).size.width / 2),
                  ),
                  border: TableBorder.all(color: Colors.blue, style: BorderStyle.solid, width: 2),
                  children: [
                    for (final item in licenseInfo.entries) buildTableRow(title: item.key, value: item.value),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  TableRow buildTableRow({required String title, required String value}) {
    return TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: [
              Text(title),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: [
              Text(value),
            ],
          ),
        ),
      ],
    );
  }
}
