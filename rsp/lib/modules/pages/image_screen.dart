import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rsp/modules/pages/cropper_screen.dart';
import 'package:rsp/components/primary_button.dart';
import 'package:image_picker/image_picker.dart';

class PhotoItem {
  final String image;
  final String name;
  PhotoItem(this.image, this.name);
}

final List<PhotoItem> _dummyItems = [
  PhotoItem(
      "https://i.pinimg.com/originals/47/66/8d/47668d007a5fa1bc78b33c51d63d3724.jpg", "Random Japan driving license"),
];

class ImageScreen extends StatefulWidget {
  const ImageScreen({Key? key}) : super(key: key);

  @override
  State<ImageScreen> createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {
  List<Uint8List> allImagesByBytes = [];
  bool isProcessing = false;
  bool isLoading = false;

  @override
  initState() {
    super.initState();
    isLoading = true;
    _loadSampleImagesFromNetwork();
  }

  Future<void> _loadSampleImagesFromNetwork() async {
    final List<Uint8List> images = [];
    for (final photoItem in _dummyItems) {
      final ByteData data = await NetworkAssetBundle(Uri.parse(photoItem.image)).load(photoItem.image);
      final Uint8List bytes = data.buffer.asUint8List();
      images.add(bytes);
    }

    setState(() {
      allImagesByBytes.addAll(images);
      isLoading = false;
    });
  }

  Future<Uint8List?> _selectImageFromGallery() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    return await image?.readAsBytes();
  }

  Future<void> _takeImageByCamera() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.camera);
    if (image == null) return;
    final imageBytes = await image.readAsBytes();
    setState(() {
      allImagesByBytes.add(imageBytes);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Sample Images')),
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * (3 / 5),
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 0,
                  mainAxisSpacing: 0,
                  crossAxisCount: 3,
                ),
                itemCount: allImagesByBytes.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CropperScreen(
                            imageBytes: allImagesByBytes[index],
                          ),
                        ),
                      );
                    },
                    child: Container(
                      child: Image.memory(Uint8List.fromList(allImagesByBytes[index])),
                    ),
                  );
                },
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  PrimaryButton(
                    onPressed: () async {
                      final Uint8List? result = await _selectImageFromGallery();
                      if (result != null) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => CropperScreen(
                              imageBytes: result,
                            ),
                          ),
                        );
                      }
                    },
                    icon: Icon(
                      Icons.folder,
                      size: 30,
                    ),
                    name: 'Gallery',
                    isProcessing: isProcessing,
                  ),
                  SizedBox(height: 20),
                  PrimaryButton(
                    onPressed: () {
                      _takeImageByCamera();
                    },
                    name: 'Take Picture',
                    icon: Icon(
                      Icons.camera,
                      size: 30,
                    ),
                    isProcessing: isProcessing,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
