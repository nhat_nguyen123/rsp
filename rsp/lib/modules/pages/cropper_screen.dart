import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_license_object_detection/flutter_license_object_detection.dart';
import 'package:rsp/components/primary_button.dart';
import 'package:rsp/modules/pages/output_screen.dart';
import 'dart:async';
import 'package:rsp/utils/google_ml_text_recognition/google_ml_text_recognition.dart';
import 'package:rsp/utils/image_manipulation/image_manipulation.dart';
import 'package:rsp/utils/jp_driving_license_object_detection/bounding_box_painter.dart';
import 'package:rsp/utils/jp_driving_license_object_detection/jp_driving_license_object_detection.dart';

class CropperScreen extends StatefulWidget {
  final Uint8List imageBytes;

  const CropperScreen({
    Key? key,
    required this.imageBytes,
  }) : super(key: key);

  @override
  State<CropperScreen> createState() => _CropperScreenState();
}

class _CropperScreenState extends State<CropperScreen> {
  bool _isDetecting = false;
  bool _isCropping = false;
  final List<Uint8List> _smallImages = [];
  final Map<LicenseInfoType, List<Uint8List>> _smallImagesMap = {};
  Size? _sourceImageSize;
  final List<LicenseInfoPrediction> _predictions = [];

  @override
  void initState() {
    super.initState();
    _isCropping = true;
    _getBoundingPainterInfo();
    _cropImageToSmallerPieces();
  }

  Future<void> _getBoundingPainterInfo() async {
    final predictions = await JPDrivingLicenseObjectDetection.getPredictions(
        imageByBytes: widget.imageBytes);

    Size? imageSize =
        await ImageManipulation.getSourceImageSize(bytes: widget.imageBytes);

    if (imageSize != null) {
      setState(() {
        _predictions.addAll(predictions);
        _sourceImageSize = imageSize;
      });
    }
  }

  Future<void> _cropImageToSmallerPieces() async {
    final detectedImages =
        await JPDrivingLicenseObjectDetection.getDetectedImages(
            imageByBytes: widget.imageBytes);

    final List<Uint8List> allImages = [];
    for (final images in detectedImages.values) {
      allImages.addAll(images);
    }

    setState(() {
      _smallImagesMap.addAll(detectedImages);
      _smallImages.addAll(allImages);
      _isCropping = false;
    });
  }

  Future<void> _processSmallImagesOCR() async {
    setState(() {
      _isDetecting = true;
    });
    final Map<LicenseInfoType, List<String>> detectedTexts = {};

    for (final imagesKVP in _smallImagesMap.entries) {
      final detectedText =
          await GoogleMLTextRecognition.detectingTextsFromImages(
              imagesKVP.value);
      if (detectedTexts.containsKey(imagesKVP.key)) {
        detectedTexts[imagesKVP.key]?.addAll(detectedText);
      } else {
        detectedTexts[imagesKVP.key] = detectedText;
      }
      print('${imagesKVP.key}: $detectedText');
    }
    setState(() {
      _isDetecting = false;
    });

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OutputScreen(
          imageBytes: widget.imageBytes,
          detectedTexts: detectedTexts,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final imageSize = _sourceImageSize;

    return Scaffold(
      appBar: AppBar(title: Text('Sample Images')),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Stack(
              children: [
                Image.memory(widget.imageBytes),
                if (imageSize != null)
                  Positioned.fill(
                    child: CustomPaint(
                      painter: BoundingBoxPainter(
                        imageSize: imageSize,
                        predictions: _predictions,
                      ),
                    ),
                  ),
              ],
            ),
          ),
          Expanded(
            child: _isCropping
                ? Center(
                    child: Text(
                      'Cropping...',
                      style: TextStyle(fontSize: 20),
                    ),
                  )
                : Container(
                    color: Colors.grey,
                    child: ListView.builder(
                      itemCount: _smallImages.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) {
                        final smallImage = _smallImages[index];

                        return Container(
                          margin: EdgeInsets.only(bottom: 30),
                          child: Column(
                            children: [
                              Container(
                                color: Colors.white,
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  children: [
                                    Text('Hello'),
                                    Image.memory(
                                      smallImage,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Center(
                  child: Opacity(
                    opacity: (_isCropping || _isDetecting) ? 0.5 : 1,
                    child: PrimaryButton(
                      isProcessing: _isDetecting,
                      onPressed: () {
                        (_isDetecting || _isCropping)
                            ? null
                            : _processSmallImagesOCR();
                      },
                      name: 'Detect Information',
                      icon: Icon(Icons.search),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
